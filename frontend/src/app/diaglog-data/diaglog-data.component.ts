import { Component, OnInit ,Inject} from '@angular/core';
import {MatDialog,MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CarListComponent } from '../car-list/car-list.component';
import { CustService } from '../shared/car/cust.service';

@Component({
  selector: 'app-diaglog-data',
  templateUrl: './diaglog-data.component.html',
  styleUrls: ['./diaglog-data.component.css']
})
export class DiaglogDataComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CarListComponent>,@Inject(MAT_DIALOG_DATA) public data: any,private custService:CustService) { }

    onNoClick(): void {
      this.dialogRef.close();
    }

    closeDialog=function(){
      this.dialogRef.close();
    }

    deleteCustomer=function(param){
      console.log('in delete function: ',param);
      this.custService.deleteCustomer(param).then(res=>{
        console.log(res);  
      });
      this.closeDialog();
    }

  ngOnInit() {
  }

}
