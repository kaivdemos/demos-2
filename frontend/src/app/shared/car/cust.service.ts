import { Injectable } from '@angular/core';
import { VariableService } from '../variables/variable.service';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Http , RequestOptions , Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CustService {

  constructor(private http: HttpClient, private UrlVariable: VariableService) { }

  /* service to add new customer */
  createCustomer(formObject){
    const ParseHeaders = {
      headers: new HttpHeaders({
      'Content-Type'  : 'application/json'
      })
    };
    const _URL = this.UrlVariable.ADDNEWCUSTOMER;
      const FormData = formObject;
      return this.http.post(_URL, FormData, ParseHeaders)
      .toPromise()
      .then(res=>res);
  }

  /* get particular customer data */
  getCustData(id){
    return this.http.get<any>(this.UrlVariable.GETCUSTOMERBYID+id)
    .toPromise()
    .then(res => res)
  }

  /* delete particular customer */ 
  deleteCustomer(id){
    const ParseHeaders = {
      headers: new HttpHeaders({
      'Content-Type'  : 'application/json'
      })
    };
    const _URL = this.UrlVariable.DELETECUSTOMERBYID+id;
      return this.http.delete(_URL)
      .toPromise()
      .then(res=>res);
  }

  /* update the customer */
  updateCustomer(id,formData){
    const ParseHeaders = {
      headers: new HttpHeaders({
      'Content-Type'  : 'application/json'
      })
    };
    const _URL = this.UrlVariable.UPDATECUSTOMERBYID+id;
    return this.http.put(_URL,ParseHeaders)
    .toPromise()
    .then(res => res);
  }
} /*end of service*/
